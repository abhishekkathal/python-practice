a = 34
b = 74
sumOfAB = a+b

print("value of sum -> ",sumOfAB)

b += a
print("value of b -> ",b)

a+=b
print("value of a -> ",a)

bool1 = True
bool2 = False

print("The value of bool1 and bool2 -> ", (bool1 and bool2))

print("The value of bool1 or bool2 -> ", (bool1 or bool2))

print("The value of not bool2 -> ", (not bool1))