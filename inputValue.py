#the value is always taken as String if it is taking input

a = input("Enter your Name: ")

print(type(a))
# To take input as an Integer we have to take Explicit Type casting to Integer

b = int(input("Enter a Number: "))

print(type(b))