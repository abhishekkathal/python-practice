# Create a list using []

from os import access


a = [1,2,3,4,5,6]

# Directly print list using print()

print(a)

# Access using Index 

print(a[0])

# Can create a list with items of different types 

b = ["asfjka",4513,"Abhishek", 54.545]

# List Slicing 

friends = ["Rahul","Prashant","Shivang","Suyash","Divya"]

print(friends[::2]) #this will take the value as [0:len(friends):2]