# File Handling 

# f = open('sample.txt','r')

# By default the mode is 'r'
# f = open('sample.txt')

# data = f.read()
# data = f.read(10) # will read the first 10 characters from the file

# readLine function 
# Will read first Line 
# data = f.readline()
# print(data)
# Will read second Line 
# data = f.readline()
# print(data)
# Will read third Line 
# data = f.readline()
# print(data)

# If the file is opened then it need to be closed at last 
# f.close()
# -------------------------------------------------------------------------


methods  = """
Modes of Opening a file 

r -> open for read
w -> open for write if file is not there then it will be created with 'w'
a -> open for append
+ -> open for update
"""

# file = open('new-file.txt','w')
# file.write("Hey, Its Abhishek How are you ?") # Will overwrite the text which has been sent using 'w'
# file.write("Hey, Its Abhishek How are you ?")
# file.write("Hey, Its Abhishek How are you ?")
# file.write("Hey, Its Abhishek How are you ?")
# file.write("Hey, Its Abhishek How are you ?")

# write() could be called any number of times


# file = open('new-file.txt','a')
# file.write("\nThis Paragraph will get appended") #Will Append the Text over the File
# file.close()
# --------------------------------------------------------------------------------
# with statment doesn't required close at the end 

with open('new-file.txt','r') as file:
   a = file.read()
print(a)
with open('new-file.txt','w') as file:
   file.write("Only I left")
# print(a)
with open('new-file.txt','r') as file:
   a = file.read()
print(a)

with open('new-file.txt','a') as file:
   file.write(" I am also Present")

with open('new-file.txt','r') as file:
   a = file.read()
print(a)



