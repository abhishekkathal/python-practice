class Employee:
   company = "MindStix"
   empCode = 401

class FreeLancer:
   company = "Pharmeasy"
   level = 1

   def upgradeLevel(self):
      self.level +=1


class Programmer(Employee,FreeLancer): #Priority of Employee will be higher
   name = "Abhishek"


Abhishek = Programmer()
print(Programmer.company) #MindStix Because the Employee class is first inherited thats why the priority of Employee class is more than Freelancer class