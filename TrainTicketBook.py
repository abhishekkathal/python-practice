class Train:

   def __init__(self,train_name,train_fare,train_seats):
      self.train_name = train_name
      self.train_fare = train_fare
      self.train_seats = train_seats

   def get_train_status(self):

      print("--------------------------------------------------\n")
      print(f"The Name of the Train is : {self.train_name}")
      print(f"Seats available in Train are : {self.train_seats}")
      print("--------------------------------------------------\n")
   
   def get_train_fare(self):

      print("--------------------------------------------------\n")
      print(f"The fare of the Train is : {self.train_fare}")
      print("--------------------------------------------------\n")
   
   def book_train_ticket(self):

      print("--------------------------------------------------\n")
      if(self.train_seats>0):
         print(f"Ticket Booked for Train : {self.train_name}")
         self.train_seats -= 1
      else:
         print(f"Sorry this train is already full! Kindly try in tatkal.")
      print("--------------------------------------------------\n")


if __name__ == '__main__':
   train1 = {1:Train("Intercity Express: 4520231",350,250)}

   train2 = {2:Train("Rajdhani Express: 156021",600,400)}

   while True:
      print("\nNo. of Trains 2")

      print("\n1. Intercity\n\n2. Rajdhani\n")

      print("Press 0 to Exit from the Application")

      try:
         user = int(input("Enter the choice of train you want to Enter : "))
         
         if(user == 0):
            break
         
         while user >0 and user<=2:
            train = train1[1] if user == 1 else train2[2]
            # print(f"Press 1 if you want to know the status of{train.train_name}")
            try:
               choice = int(input(f"""Press 1 : To get Status of {train.train_name}
Press 2 : To know the Fare of {train.train_name}
Press 3 : To book the ticket of {train.train_name}
Press 0 : If you want to go to the previous menu
Enter your Choice : """))
               if(choice == 0):
                  break

               if(choice == 1):
                  train.get_train_status()

               if(choice == 2):
                  train.get_train_fare()

               if(choice == 3):
                  train.book_train_ticket()

            except ValueError:
               print("==========================================================================\n")
               print("Invalid Input please give a valid input")
               # break
         else:
            print("The Train is not in the list... Please give the input for the train that is present")

      except ValueError:
         print("==========================================================================\n")
         print("Invalid Input please give a valid input")

      finally:
         print("==========================================================================\n")




