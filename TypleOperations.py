someTuple = (1,2,1,5,8,4,5,2,3,5,8,96,5,10,100,10,50,51,62,42,4,5458,45,45,1,"Abhishek","Kathal","Mustang","lamborgini","how",35,8,4,45,4)


# Iterate through tuple  

for i in range(len(someTuple)):
   print(someTuple[i],end=" ")

# Find index of something in tuple 

print("\nIndex of 5458 is -> {}".format(someTuple.index(5458)))

# Count no. of occurance of a something in Tuple 

print(someTuple.count(1))

# Tuple to list 

someTuple = list(someTuple)

print(someTuple)

someTuple = tuple(someTuple)

# Remove all duplicate value from Tuple

someTuple = tuple(dict.fromkeys(someTuple))

print(someTuple)


