import functools

def simpleArraySum(ar):
    x = functools.reduce(lambda a,b : a+b,ar)
    return x

print(("The Sum of list is : {} ").format(simpleArraySum([5,7,6,2,8])))

func1 = lambda x : x+x

def func2(x):
   return x + x

print(func1(5))

# Checking wheather this will work or not !!!
x = "Hello"[0]
print(x)