class Employee:
   
   company = "MindStix"
   workType = "Work From Home"
   
   def __init__(self,name,loc): 
      self.name = name
      self.loc = loc
      print(f"Hello {self.name} Welcome to {self.company}!")
      self.greet()
   # Learning the use of Self 
   def getDetails(self):
      return f"Employee Name : {self.name}\nEmployee Location: {self.loc}\nCompany : {self.company}\nWork Type : {self.workType}"

   @staticmethod
   def greet():
      print(f"Good Morning Sir!")
# Instantiating the Employee Object 

ambi = Employee("Ambikeshwar Singh","Pune") 
# Changing the Defaulf work Location
ambi.workType = "On Site"
# ambi.greet()   

print(ambi.getDetails()) # == Employee.getDetails(ambi)




# Below Line will throw Error because salary Attribute is not present in the Object Employee or in its instance 

# print(ankita.salary)

