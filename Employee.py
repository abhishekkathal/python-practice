class Employee:
   
   company = "MindStix"
   workType = "Work From Home"
   
   def __init__(self,name,loc): #Works as a Constructor once the object is instantiated this will automatically work 

      self.name = name
      self.loc = loc
      print(f"Hello {self.name} Welcome to {self.company}!")
   # Learning the use of Self 
   def getDetails(self):
      return f"Employee Name : {self.name}\nEmployee Location: {self.loc}\nCompany : {self.company}\nWork Type : {self.workType}"
# Instantiating the Employee Object 

abhishek = Employee("Abhishek Kathal","Bhopal")

mukesh = Employee("Mukesh Kumar","Bihar")

ambi = Employee("Ambikeshwar Singh","Pune") 
# Changing the Defaulf work Location
ambi.workType = "On Site"

ankita = Employee("Ankita Purekar","Pune") 

print("------------------------------------------------------------")

print(abhishek.getDetails())

print("------------------------------------------------------------")

print(mukesh.getDetails())

print("------------------------------------------------------------")

print(ambi.getDetails()) # == Employee.getDetails(ambi)

print("------------------------------------------------------------")

print(ankita.getDetails())



# Below Line will throw Error because salary Attribute is not present in the Object Employee or in its instance 

# print(ankita.salary)

