a = "3457564"

print("Type of a is -> ",type(a))

# Explicit Type Casting

print("Addition of String and Integer -> ",int(a)+456)

# But only the things which could be changed to integer could be type cast 

b = 31

b = str(b)

print("Number can be converted to the String like ->",b)

print("Type of b is now  ->",type(b))

