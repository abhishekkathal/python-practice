# from typing import ItemsView, Iterable

# print(type(1))


# listOfElem = [1,2,3,'a','b','c']

# # Check if a list contains an element

# print("--------------------------------------")

# if 'a' in listOfElem:
#    print("Yes Element is in listOfElem")
# else:
#    print("No Element is not in listOfElem")

# print("--------------------------------------")

# # Id of listOfElem
# print(id(listOfElem))

# # Iterate in listOfElem

# print("--------------------------------------")

# for i in listOfElem:
#    print(i)

# # Iterate with spaces 
# print("--------------------------------------")


# for i in listOfElem:
#    print(i,end=" ")

# # Iterate with Indexes

# print("\n--------------------------------------")

# for i in range(len(listOfElem)):
#    print("Value of Index {} -> ".format(i),listOfElem[i])

# # Adding new Items in List 

# listOfElem.append(54)
# listOfElem.append(58)
# listOfElem.append('d')
# listOfElem.append('q')
# listOfElem.append('Something New')
# listOfElem.append(135465)


# # Adding through Input till value is inserted as "0" only Integer
# n = None

# print("Enter 0 if want to Exit")

# while(n!=0):
#    if n!=0:
#       n = int(input("Enter Any number only : "))
#       listOfElem.append(n)

# print(listOfElem)

# n = None
# print("Enter 0 or No if want to exit")


# while( n!='0' and n!='No'):
#    n = input("Enter anything we will store it in list : ")
#    if(n != '0' and n!='No' ):
#       if(n.isdigit()):
#          listOfElem.append(int(n))
#       else:
#          listOfElem.append(n)


# print(listOfElem)


# #Removing Items from specific Index of the List 

# print("Remove Item from the Specific index of the list\nThe list Indexing Starts from 0 to {}".format(len(listOfElem)))

# n = int(input("How many Element you want to Delete ?, Please Enter : "))

# while (n):
#    print("Range of List index is from 0 to {}".format(len(listOfElem)))
#    idx = int(input("Enter the index from which you  want to remove Element : "))
#    listOfElem.pop(idx)
#    n -= 1

# print(listOfElem)


# if(input("Do you want to Clear the list ? Type Yes or No : ")=='Yes'):
#    listOfElem.clear()

# # Multiple whole List Integer Element of List

# multiply = lambda x,y:x*y

# mul = 1

# for i in range(len(listOfElem)):
#    if(str(i).isdigit() and i!=0):
#       mul = multiply(mul,i)
#       # print(type(i))
   

# print("Multiplication of all the integers of list is : {}".format(mul))

# #Reversed List

# listOfElem.reverse()

# print(listOfElem)


# # Count the element of the list 

# print(listOfElem.count("a"))


listOfInt = [3,4,57,85,3,4,6,44]

newList = [val for val in listOfInt if val%2==0]

print(newList)


flag = 457353

# flag = "String value"

num = 2 if flag else 3

print(num)

print(type(flag))


flag = str(flag)

print(type(flag))

if(type(flag) is str) :
   print("Its a string")
else:
   print("Its something else")






