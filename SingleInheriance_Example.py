class Employee:
   
   company = "MindStix"
   workType = "Work From Home"
   
   def __init__(self,name,loc): #Works as a Constructor once the object is instantiated this will automatically work 

      self.name = name
      self.loc = loc
      print(f"Hello {self.name} Welcome to {self.company}!")
   # Learning the use of Self 
   def getDetails(self):
      return f"Employee Name : {self.name}\nEmployee Location: {self.loc}\nCompany : {self.company}\nWork Type : {self.workType}"

class Programmer(Employee):
   language = "Python"

   def getLanguage(self):
      print(f"{self.name} uses the {self.language} for Coding")





# abhishek = Employee("Abhishek","Bhopal")

progAbhishek = Programmer("Abhishek","Bhopal")


# print(abhishek.getDetails())
print(progAbhishek.getDetails())