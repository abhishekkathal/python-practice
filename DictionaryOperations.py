myDict = {
   "key1":"Value1",
   "key2":"Value2",
   "key3":"Value3",
   "key4":"Value4",
   "key5":"Value5",
   "key6":"Value6",
   
}

newDict = {
   "key7": {
      "1":"one",
      "2":"two",
      "3":"three",
      "4":"four",
      "5":"five",
   }
}

# Update a dictionary inside dictionary


myDict.update(newDict)

print(myDict)

# Get all the key in a single list 

li = myDict.keys()

print(li)

# Removing the element with the specific key 

myDict.pop("key7")

print(myDict)

#Removing the  last inserted key-value pair

myDict.popitem()

print(myDict)

# Get all the values of list

print(myDict.values())

# Iterate through Dictionary 

for i in myDict:
   print(myDict[i])

# Iterate through Dictionary with keys and value pair

for x,y in myDict.items(): #items() return a list containing tuple of key value
   print("value at {} is -> {}".format(x,y))


# Iterate Dictionary using get

for i in myDict:
   print(myDict.get(i))

print("-----------------------------------------------------")

newTuple = (2,5,[1,2],8,2,56,45)

print(newTuple[2])

newTuple[2][0] = 10

# newTuple[]

print()

print(newTuple[2])

print()



newTuple1 = (1,)

print(newTuple1)

