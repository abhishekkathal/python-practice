from typing import Counter


class Person:
   Country = "India"

   def eats(self):
      print("I am Eating")

class Employee:
   company = "MindStix"

   def getCompanyDetails(self):
      print(f"Works in {self.company}")